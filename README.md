# miniconda2 python 2.7 Singularity container
Based on debian 9.8 slim<br>

Singularity container based on the recipe: Singularity.miniconda2-py27_4.8.3.def

Package installation using Miniconda2 python2 2.7 version miniconda2-py27_4.8.3 <br>

Used as base to build containers<br>
- local image, header:<br>
	boostrap:localimage <br>
	from:miniconda2-py27_4.8.3.sif <br>
- Or via oras, header: <br>
        boostrap:oras <br>
        from:oras://registry.forgemia.inra.fr/singularity/prebuilt/miniconda2-py27/miniconda2-py27:latest <br>

Can be used in a Multi-Stage build (pack is installed) <br>


usage:<br>
     miniconda2-py27_4.8.3.sif --help


Image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>
```bash
singularity pull miniconda2-py27_4.8.3.sif oras://registry.forgemia.inra.fr/singularity/prebuilt/miniconda2-py27/miniconda2-py27:latest
```

For local build:
```bash
sudo singularity build miniconda2-py27_4.8.3.sif Singularity.miniconda2-py27_4.8.3.def
```


contact: jacques.lagnel@inrae.fr

